#!/bin/bash
VAULT_PASSWORD=''
SERVER_IP=''
BECOME_PASSWORD=''
REMOTE_USER=''

OPENSEARCH_VERSION='2.6.0'
OPENSEARCH_DASHBOARD_VERSION='2.6.0'
LOGSTASH_VERSION='8.6.1'
FILEBEAT_VERSION='7.12.1'

echo "$VAULT_PASSWORD" > VAULT_PASS.txt

sed -i "s/<ip_адрес_сервера_opensearch>/$SERVER_IP/" ./hosts
sed -i "s/<ip_адрес_сервера_filebeat>/$SERVER_IP/" ./hosts

ansible-vault encrypt_string --vault-password-file VAULT_PASS.txt --name 'ansible_become_pass' "$BECOME_PASSWORD" \
                             >> ./host_vars/opensearch-1/var.yaml
ansible-vault encrypt_string --vault-password-file VAULT_PASS.txt --name 'ansible_become_pass' "$BECOME_PASSWORD" \
                             >> ./host_vars/filebeat-1/var.yaml

sed -i "s/<имя_пользователя>/$REMOTE_USER/" ./host_vars/opensearch-1/var.yaml
sed -i "s/<имя_пользователя>/$REMOTE_USER/" ./host_vars/filebeat-1/var.yaml

sed -i "s/2.6.0/$OPENSEARCH_VERSION/" ./roles/install_opensearch/defaults/main.yaml
sed -i "s/2.6.0/$OPENSEARCH_DASHBOARD_VERSION/" ./roles/install_opensearch_dashboards/defaults/main.yaml
sed -i "s/8.6.1/$LOGSTASH_VERSION/" ./roles/install_logstash/defaults/main.yaml
sed -i "s/7.12.1/$FILEBEAT_VERSION/" ./roles/install_filebeat/defaults/main.yaml

# sed -i -e '$a\' ./host_vars/opensearch-1/var.yaml && \
# ansible-vault encrypt_string --vault-password-file VAULT_PASS.txt --name 'ADMIN_PASSWORD_OPENSEARCH' "$ADMIN_PASSWORD" \
#                             >> ./host_vars/opensearch-1/var.yaml






